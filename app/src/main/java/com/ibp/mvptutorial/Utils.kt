package com.ibp.mvptutorial

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Predicate
import io.reactivex.observers.TestObserver

fun EditText.addSimpleTextChangedListener(fn: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable) {
            fn(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    })
}

fun Disposable.disposedBy(bag: CompositeDisposable) = bag.add(this)


fun <T> TestObserver<T>.assertLastValue(p: Predicate<T>) = assertValueAt(valueCount() - 1, p)

fun <T> TestObserver<T>.assertLastValue(item: T) = assertValueAt(valueCount() - 1, { it == item })

fun <T, U, R> Observable<T>.withLatestFromK(other: Observable<U>, combine: (T, U) -> R) =
    withLatestFrom(other, BiFunction(combine))

