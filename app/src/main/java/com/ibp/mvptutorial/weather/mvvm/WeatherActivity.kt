package com.ibp.mvptutorial.weather.mvvm

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.instance
import com.ibp.mvptutorial.R
import com.ibp.mvptutorial.addSimpleTextChangedListener
import com.ibp.mvptutorial.disposedBy
import com.ibp.mvptutorial.weather.models.WeatherData
import com.squareup.picasso.Picasso
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Alexandre Delattre on 28/06/2017.
 */

class WeatherActivity : KodeinAppCompatActivity() {

    val viewModel: WeatherViewModel by instance()

    lateinit var progressBar: ProgressBar
    lateinit var searchEditText: EditText
    lateinit var searchButton: Button

    lateinit var weatherLayout: View
    lateinit var imageView: ImageView
    lateinit var textViewMinTemp: TextView
    lateinit var textViewMaxTemp: TextView

    val disposeBag = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUi()
        initBindings()
    }

    fun initUi() {
        progressBar = findViewById(R.id.progressBar) as ProgressBar
        progressBar.visibility = View.GONE
        searchEditText = findViewById(R.id.editText) as EditText
        searchButton = findViewById(R.id.button) as Button
        weatherLayout = findViewById(R.id.weatherLayout)
        imageView = findViewById(R.id.imageView) as ImageView
        textViewMinTemp = findViewById(R.id.textViewMinTemp) as TextView
        textViewMaxTemp = findViewById(R.id.textViewMaxTemp) as TextView
    }

    fun initBindings() {
        searchEditText.addSimpleTextChangedListener {
            viewModel.city.onNext(it)
        }

        searchEditText.setOnEditorActionListener { _, _, _ ->
            viewModel.startSearch.onNext(Unit)
            false
        }

        searchButton.setOnClickListener {
            dismissKeyboard()
            viewModel.startSearch.onNext(Unit)
        }

        viewModel.searchEnabled.subscribe {
            searchButton.isEnabled = it
        }.disposedBy(disposeBag)

        viewModel.state.subscribe {
            when (it) {
                WeatherViewModel.State.Idle -> {
                    displayLoader(false)
                    displayWeatherLayout(true)
                }
                WeatherViewModel.State.Loading -> {
                    displayLoader(true)
                    displayWeatherLayout(false)
                }
                is WeatherViewModel.State.Error -> {
                    displayError(it.e)
                    displayLoader(false)
                }
            }
        }.disposedBy(disposeBag)

        viewModel.weather.subscribe {
            displayWeather(it)
        }.disposedBy(disposeBag)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposeBag.clear()
    }


    fun displayLoader(loading: Boolean) {
        progressBar.visibility = if (loading) View.VISIBLE else View.GONE
    }

    fun displayWeatherLayout(visible: Boolean) {
        weatherLayout.visibility = if (visible) View.VISIBLE else View.GONE
    }

    fun displayWeather(data: WeatherData) {
        Picasso.with(this).load(data.pictoUrl).into(imageView)
        textViewMinTemp.text = "${data.minTemperature} °"
        textViewMaxTemp.text = "${data.maxTemperature} °"
    }

    fun displayError(e: Throwable) {
        Snackbar.make(searchButton, "Erreur : ${e.localizedMessage}", Snackbar.LENGTH_SHORT).show()
    }

    private fun dismissKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(searchEditText.windowToken, 0)
    }
}


