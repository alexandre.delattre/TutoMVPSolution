package com.ibp.mvptutorial.weather.services.geocode

import io.reactivex.Single

class GeocodeServiceImpl(val key: String, val geocoder: GoogleGeocodeApi) : GeocodeService {

    override fun simpleGeocode(address: String): Single<GeocodeResult> {
        return geocoder.geocode(key, address).flatMap { resp ->
            val bestResult = resp.results.firstOrNull()
            if (bestResult != null) {
                val city = bestResult.address_components.find { it.types.contains("locality") }?.short_name
                if (city != null) {
                    Single.just(GeocodeResult(city, bestResult.geometry.location))
                } else Single.error(CannotGeocodeException(address))
            } else Single.error(CannotGeocodeException(address))
        }
    }

}