package com.ibp.mvptutorial.weather.ui

import android.content.Context
import android.support.design.widget.Snackbar
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import com.ibp.mvptutorial.mvp.MvpActivity
import com.ibp.mvptutorial.R
import com.ibp.mvptutorial.addSimpleTextChangedListener
import com.ibp.mvptutorial.weather.models.WeatherData
import com.ibp.mvptutorial.weather.ui.WeatherContracts.WeatherPresenter
import com.ibp.mvptutorial.weather.ui.WeatherContracts.WeatherView
import com.squareup.picasso.Picasso


class WeatherActivity : MvpActivity<WeatherView>(), WeatherView {

    override val layout = R.layout.activity_main
    override val presenter by with(this).instance<WeatherPresenter>()

    lateinit var progressBar: ProgressBar
    lateinit var searchEditText: EditText
    lateinit var searchButton: Button

    lateinit var weatherLayout: View
    lateinit var imageView: ImageView
    lateinit var textViewMinTemp: TextView
    lateinit var textViewMaxTemp: TextView

    override fun displayLoader(loading: Boolean) {
        progressBar.visibility = if (loading) View.VISIBLE else View.GONE
    }

    override fun enableSearchButton(enabled: Boolean) {
        searchButton.isEnabled = enabled
    }

    override fun displayWeatherLayout(visible: Boolean) {
        weatherLayout.visibility = if (visible) View.VISIBLE else View.GONE
    }

    override fun displayWeather(data: WeatherData) {
        Picasso.with(this).load(data.pictoUrl).into(imageView)
        textViewMinTemp.text = "${data.minTemperature} °"
        textViewMaxTemp.text = "${data.maxTemperature} °"
    }

    override fun displayError(e: Throwable) {
        Snackbar.make(searchButton, "Erreur : ${e.localizedMessage}", Snackbar.LENGTH_SHORT).show()
    }

    override fun initUi() {
        progressBar = findViewById(R.id.progressBar) as ProgressBar
        progressBar.visibility = View.GONE

        searchEditText = findViewById(R.id.editText) as EditText
        searchEditText.addSimpleTextChangedListener {
            presenter.city = it
        }

        searchEditText.setOnEditorActionListener { _, _, _ ->
            presenter.searchWeather()
            false
        }

        searchButton = findViewById(R.id.button) as Button
        searchButton.setOnClickListener {
            dismissKeyboard()
            presenter.searchWeather()
        }

        weatherLayout = findViewById(R.id.weatherLayout)
        imageView = findViewById(R.id.imageView) as ImageView
        textViewMinTemp = findViewById(R.id.textViewMinTemp) as TextView
        textViewMaxTemp = findViewById(R.id.textViewMaxTemp) as TextView
    }

    private fun dismissKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(searchEditText.windowToken, 0)
    }
}

