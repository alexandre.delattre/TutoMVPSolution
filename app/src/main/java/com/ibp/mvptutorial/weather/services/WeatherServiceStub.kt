package com.ibp.mvptutorial.weather.services

import com.ibp.mvptutorial.weather.models.WeatherData
import io.reactivex.Single
import java.util.concurrent.TimeUnit

class WeatherServiceStub : WeatherService {
    private fun getWeatherForCity(city: String) = when (city) {
        "toulouse" -> WeatherData(city, "http://openweathermap.org/img/w/01d.png", 20f, 35f)
        "paris" -> WeatherData(city, "http://openweathermap.org/img/w/13d.png", -10f, 0f)
        else -> WeatherData(city, "http://openweathermap.org/img/w/03d.png", 15f, 20f)
    }
    override fun getWeather(city: String): Single<WeatherData> {
        if (city != "error") {
            return Single.just(getWeatherForCity(city)).delay(2, TimeUnit.SECONDS)
        } else {
            return Single.error<WeatherData>(Exception("Impossible d'obtenir la météo"))
                    .delaySubscription<WeatherData>(2, TimeUnit.SECONDS)
        }
    }
}