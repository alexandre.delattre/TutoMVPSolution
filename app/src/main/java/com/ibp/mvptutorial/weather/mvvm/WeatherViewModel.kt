package com.ibp.mvptutorial.weather.mvvm

import com.ibp.mvptutorial.weather.models.WeatherData
import com.ibp.mvptutorial.weather.services.WeatherService
import com.ibp.mvptutorial.withLatestFromK
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class WeatherViewModel(val weatherService: WeatherService, val mainScheduler: Scheduler) {

    sealed class State {
        object Idle : State()
        object Loading : State()
        data class Error(val e: Throwable) : State()
    }

    // Inputs
    val city: Subject<String>
    val startSearch: Subject<Unit>

    // Outputs
    val state: Observable<State>
    val searchEnabled: Observable<Boolean>
    val weather: Observable<WeatherData>

    init {
        city = BehaviorSubject.createDefault("")
        startSearch = PublishSubject.create()

        state = BehaviorSubject.createDefault(State.Idle)
        searchEnabled = Observable.combineLatest(city, state, BiFunction {
            city, state -> city.isNotEmpty() && state != State.Loading
        })

        weather = startSearch.withLatestFromK(city) { _, city -> city}
                .filter { it.isNotEmpty() && state.value != State.Loading }
                .doOnNext { state.onNext(State.Loading) }
                .switchMap {
                    weatherService.getWeather(it)
                        .observeOn(mainScheduler)
                        .doOnSuccess { state.onNext(State.Idle)  }
                        .doOnError { state.onNext(State.Error(it)) }
                        .toObservable()
                        .onErrorResumeNext(Observable.empty())
                }

    }
}