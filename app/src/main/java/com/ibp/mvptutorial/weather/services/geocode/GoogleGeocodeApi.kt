package com.ibp.mvptutorial.weather.services.geocode

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface GoogleGeocodeApi {
    data class Location(val lat: Double, val lng: Double)
    data class Geometry(val location: Location)
    data class AddressComponent(val long_name: String,
                                val short_name: String,
                                val types: List<String>)
    data class Result(val address_components: List<AddressComponent>,
                      val formatted_address: String,
                      val geometry: Geometry)
    data class GeocodeResponse(val status: String, val results: List<Result>)

    @GET("/maps/api/geocode/json")
    fun geocode(@Query("key") key: String, @Query("address") address: String): Single<GeocodeResponse>
}