package com.ibp.mvptutorial.weather.models

data class WeatherData(
        val city: String,
        val pictoUrl: String,
        val minTemperature: Float,
        val maxTemperature: Float
)