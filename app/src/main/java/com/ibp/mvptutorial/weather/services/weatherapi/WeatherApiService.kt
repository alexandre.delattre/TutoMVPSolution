package com.ibp.mvptutorial.weather.services.weatherapi

import com.ibp.mvptutorial.weather.models.WeatherData
import io.reactivex.Single

interface WeatherApiService {
    fun getWeather(latitude: Double, longitude: Double): Single<WeatherData>
}