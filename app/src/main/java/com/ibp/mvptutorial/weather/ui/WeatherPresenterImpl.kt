package com.ibp.mvptutorial.weather.ui

import com.ibp.mvptutorial.mvp.BasePresenter
import com.ibp.mvptutorial.weather.services.WeatherService
import com.ibp.mvptutorial.weather.ui.WeatherContracts.WeatherPresenter
import com.ibp.mvptutorial.weather.ui.WeatherContracts.WeatherView
import io.reactivex.Scheduler

class WeatherPresenterImpl(val weatherService: WeatherService, val scheduler: Scheduler)
    : BasePresenter<WeatherView>(), WeatherPresenter {

    private var loading = false
        set(value) {
            field = value
            updateSearchButton()
            view?.displayLoader(value)
            view?.displayWeatherLayout(!value)
        }

    private var disposeBag = io.reactivex.disposables.CompositeDisposable()

    override fun create() {
        super.create()
        updateSearchButton()
    }

    override fun destroy() {
        super.destroy()
        disposeBag.clear()
    }

    override var city: String = ""
        set(value) {
            field = value
            updateSearchButton()
        }

    override fun searchWeather() {
        if (city.isNotEmpty()) {
            loading = true
            disposeBag.add(weatherService.getWeather(city)
                    .observeOn(scheduler)
                    .subscribe({ data ->
                        loading = false
                        view?.displayWeather(data)
                    }, { err ->
                        loading = false
                        view?.displayWeatherLayout(false)
                        view?.displayError(err)
                    }))
        }
    }

    private fun updateSearchButton() {
        view?.enableSearchButton(city.isNotEmpty() && !loading)
    }
}