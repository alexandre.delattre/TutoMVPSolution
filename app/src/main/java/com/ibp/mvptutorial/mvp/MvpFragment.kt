package com.ibp.mvptutorial.mvp

import android.os.Bundle
import android.support.v4.app.Fragment

/**
 * Created by itudenj on 08/06/2017.
 */
abstract class MvpFragment<V : MvpView> : Fragment(), MvpView {

    abstract val presenter: MvpPresenter<V>?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        @Suppress("UNCHECKED_CAST")
        presenter?.view = this as V
        presenter?.create()
    }

    override fun onStart() {
        super.onStart()
        presenter?.start()
    }

    override fun onStop() {
        super.onStop()
        presenter?.stop()
    }

    override fun onDestroy() {
        presenter?.destroy()
        super.onDestroy()
    }
}