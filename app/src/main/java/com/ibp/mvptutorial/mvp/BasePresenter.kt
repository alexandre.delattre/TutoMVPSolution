package com.ibp.mvptutorial.mvp

/**
 * Base presenter implementation
 *
 */
abstract class BasePresenter<V : MvpView> : MvpPresenter<V> {

    override var view: V? = null

    override fun create() {
    }

    override fun start() {
    }

    override fun stop() {
    }

    override fun destroy() {
    }
}