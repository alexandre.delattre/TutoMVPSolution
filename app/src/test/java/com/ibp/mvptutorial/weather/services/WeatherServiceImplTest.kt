package com.ibp.mvptutorial.weather.services

import com.ibp.mvptutorial.weather.models.WeatherData
import com.ibp.mvptutorial.weather.services.geocode.GeocodeResult
import com.ibp.mvptutorial.weather.services.geocode.GeocodeService
import com.ibp.mvptutorial.weather.services.geocode.GoogleGeocodeApi
import com.ibp.mvptutorial.weather.services.weatherapi.WeatherApiService
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito.*
import java.util.concurrent.TimeUnit

/**
 * Created by Alexandre Delattre on 22/06/2017.
 */
class WeatherServiceImplTest {
    lateinit var geocodeService: GeocodeService
    lateinit var weatherApiService: WeatherApiService
    lateinit var weatherService: WeatherServiceImpl
    lateinit var scheduler: TestScheduler

    @Before
    fun setUp() {
        geocodeService = mock(GeocodeService::class.java)
        weatherApiService = mock(WeatherApiService::class.java)
        weatherService = WeatherServiceImpl(geocodeService, weatherApiService)
        scheduler = TestScheduler()
    }

    val geocodeResult = GeocodeResult("toulouse", GoogleGeocodeApi.Location(1.2, 3.4))
    val weatherData = WeatherData("toulouse", "picto", 10f, 20f)

    @Test
    fun testNominal() {
        `when`(geocodeService.simpleGeocode("toulouse"))
                .thenReturn(Single.just(geocodeResult).delay(3, TimeUnit.SECONDS, scheduler))
        `when`(weatherApiService.getWeather(1.2, 3.4))
                .thenReturn(Single.just(weatherData).delay(2, TimeUnit.SECONDS, scheduler))

        val testObserver = TestObserver<WeatherData>()
        weatherService.getWeather("toulouse").subscribe(testObserver)

        scheduler.advanceTimeTo(5, TimeUnit.SECONDS)
        testObserver.assertValue(weatherData)
        testObserver.assertComplete()
    }

    @Test
    fun testGeocodeError() {
        val geocodeException = Exception()
        `when`(geocodeService.simpleGeocode("toulouse"))
                .thenReturn(Single.error<GeocodeResult>(geocodeException)
                        .delaySubscription<GeocodeResult>(2, TimeUnit.SECONDS, scheduler))
        `when`(weatherApiService.getWeather(1.2, 3.4))
                .thenReturn(Single.just(weatherData).delay(2, TimeUnit.SECONDS, scheduler))

        val testObserver = TestObserver<WeatherData>()
        weatherService.getWeather("toulouse").subscribe(testObserver)

        scheduler.advanceTimeTo(2, TimeUnit.SECONDS)
        testObserver.assertError(geocodeException)
    }

    @Test
    fun testWeatherError() {
        val weatherException = Exception()
        `when`(geocodeService.simpleGeocode("toulouse"))
                .thenReturn(Single.just(geocodeResult).delay(3, TimeUnit.SECONDS, scheduler))
        `when`(weatherApiService.getWeather(1.2, 3.4))
                .thenReturn(Single.error<WeatherData>(weatherException)
                        .delaySubscription<WeatherData>(2, TimeUnit.SECONDS, scheduler))

        val testObserver = TestObserver<WeatherData>()
        weatherService.getWeather("toulouse").subscribe(testObserver)

        scheduler.advanceTimeTo(5, TimeUnit.SECONDS)
        testObserver.assertError(weatherException)
    }
}