package com.ibp.mvptutorial.weather.mvvm

import com.ibp.mvptutorial.assertLastValue
import com.ibp.mvptutorial.weather.models.WeatherData
import com.ibp.mvptutorial.weather.services.WeatherService
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.util.concurrent.TimeUnit

/**
 * Created by Alexandre Delattre on 28/06/2017.
 */
class WeatherViewModelTest {
    lateinit var weatherService: WeatherService
    lateinit var viewModel: WeatherViewModel
    lateinit var scheduler: TestScheduler

    lateinit var searchEnabledObserver: TestObserver<Boolean>
    lateinit var stateObserver: TestObserver<WeatherViewModel.State>
    lateinit var weatherObserver: TestObserver<WeatherData>

    val weatherData = WeatherData("toulouse", "", 10f, 20f)

    @Before
    fun setup() {
        weatherService = Mockito.mock(WeatherService::class.java)
        scheduler = TestScheduler()
        viewModel = WeatherViewModel(weatherService, scheduler)

        searchEnabledObserver = TestObserver()
        stateObserver = TestObserver()
        weatherObserver = TestObserver()

        viewModel.searchEnabled.subscribe(searchEnabledObserver)
        viewModel.state.subscribe(stateObserver)
        viewModel.weather.subscribe(weatherObserver)
    }

    @Test
    fun testLaunch() {
        // Given a weather app view model
        // When the screen is started
        // Then the search button is disabled and the state is in Idle state
        searchEnabledObserver.assertLastValue(false)
        stateObserver.assertLastValue(WeatherViewModel.State.Idle)

        // When the user type toulouse, after 1 seconds
        scheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        viewModel.city.onNext("toulouse")
        // Then the search button is enabled, and state is idle
        searchEnabledObserver.assertLastValue(true)
        stateObserver.assertLastValue(WeatherViewModel.State.Idle)


        // When the user click on start search,
        scheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        viewModel.startSearch.onNext(Unit)
        // Then the search button is disabled, the state is loading, and weather service was invoked
        searchEnabledObserver.assertLastValue(false)
        stateObserver.assertLastValue(WeatherViewModel.State.Loading)
        Mockito.verify(weatherService).getWeather("toulouse")
        weatherObserver.assertNoValues()
    }


    @Test
    fun testSuccess() {

        Mockito.`when`(weatherService.getWeather("toulouse"))
                .thenReturn(Single.just(weatherData).delay(5, TimeUnit.SECONDS, scheduler))

        testLaunch()

        // When after 5 seconds, the service returns a data
        scheduler.advanceTimeBy(5, TimeUnit.SECONDS)
        // Then the search button is enabled again, the state is idle, the weather is displayed
        searchEnabledObserver.assertLastValue(true)
        stateObserver.assertLastValue(WeatherViewModel.State.Idle)
        weatherObserver.assertLastValue(weatherData)
    }


    @Test
    fun testFailure() {
        val error = Exception()
        Mockito.`when`(weatherService.getWeather("toulouse"))
                .thenReturn(Single.error<WeatherData>(error)
                        .delaySubscription<WeatherData>(5, TimeUnit.SECONDS, scheduler))

        testLaunch()

        // When after 5 seconds, the service returns an error
        scheduler.advanceTimeBy(5, TimeUnit.SECONDS)
        // Then the search button is enabled again, the state is error, the weather is not displayed
        searchEnabledObserver.assertLastValue(true)
        stateObserver.assertLastValue(WeatherViewModel.State.Error(error))
        weatherObserver.assertNoValues()
    }

    @Test
    fun testRetry() {
        val error = Exception()
        Mockito.`when`(weatherService.getWeather("toulouse"))
                .thenReturn(Single.error<WeatherData>(error)
                        .delaySubscription<WeatherData>(5, TimeUnit.SECONDS, scheduler))
                .thenReturn(Single.just(weatherData).delay(5, TimeUnit.SECONDS, scheduler))

        testLaunch()

        // When after 5 seconds, the service returns an error
        scheduler.advanceTimeBy(5, TimeUnit.SECONDS)
        // Then the search button is enabled again, the state is error, the weather is not displayed
        searchEnabledObserver.assertLastValue(true)
        stateObserver.assertLastValue(WeatherViewModel.State.Error(error))
        weatherObserver.assertNoValues()

        // When the user click the search button again
        viewModel.startSearch.onNext(Unit)
        // Then after 5 seconds, the weather is displayed
        scheduler.advanceTimeBy(5, TimeUnit.SECONDS)
        stateObserver.assertLastValue(WeatherViewModel.State.Idle)
        weatherObserver.assertLastValue(weatherData)

    }
}