package com.ibp.mvptutorial.weather.ui

import com.ibp.mvptutorial.weather.models.WeatherData
import com.ibp.mvptutorial.weather.services.WeatherService
import com.ibp.mvptutorial.weather.ui.WeatherContracts.WeatherView
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import java.util.concurrent.TimeUnit

class WeatherPresenterImplTest {
    lateinit var view: WeatherView
    lateinit var weatherService: WeatherService
    lateinit var presenter: WeatherPresenterImpl
    lateinit var scheduler: TestScheduler

    @Before
    fun setup() {
        view = mock(WeatherView::class.java)
        weatherService = mock(WeatherService::class.java)
        scheduler = TestScheduler()
        presenter = WeatherPresenterImpl(weatherService, scheduler)
        presenter.view = view
    }

    @Test
    fun testCreation() {
        // Given a weather presenter
        // When the screen is created
        // Then the search button is disabled
        presenter.create()
        verify(view).enableSearchButton(false)
    }

    @Test
    fun testEnable() {
        // Given a weather presenter
        // When the user enter a city
        // The search button is enabled
        presenter.create()
        presenter.city = "toulouse"
        verify(view).enableSearchButton(true)
    }

    @Test
    fun testSearchWeatherSuccess() {
        // Given a weather presenter
        // When the user enter a city and press the search button
        val weatherData = WeatherData("toulouse", "", 10f, 20f)
        `when`(weatherService.getWeather("toulouse")).thenReturn(Single.just(weatherData).delay(5, TimeUnit.SECONDS, scheduler))
        presenter.create()
        presenter.city = "toulouse"
        presenter.searchWeather()

        // Then a loader is shown
        verify(view).displayLoader(true)

        // And after 5 seconds, the weather is displayed
        scheduler.advanceTimeBy(5, TimeUnit.SECONDS)
        verify(view).displayWeather(weatherData)
        verify(view).displayLoader(false)

    }

    @Test
    fun testSearchWeatherError() {
        // Given a weather presenter
        // When the user enter a city and press the search button
        val error = Single.error<WeatherData>(Exception())
                .delaySubscription<WeatherData>(5, TimeUnit.SECONDS, scheduler)

        `when`(weatherService.getWeather("error")).thenReturn(error)
        presenter.create()
        presenter.city = "error"
        presenter.searchWeather()

        // Then a loader is shown
        verify(view).displayLoader(true)

        // And after 5 seconds, an error is shown
        scheduler.advanceTimeBy(5, TimeUnit.SECONDS)
        verify(view).displayLoader(false)

    }
}
