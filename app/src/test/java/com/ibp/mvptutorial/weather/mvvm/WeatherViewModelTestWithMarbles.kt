package com.ibp.mvptutorial.weather.mvvm

import com.ibp.mvptutorial.assertLastValue
import com.ibp.mvptutorial.weather.models.WeatherData
import com.ibp.mvptutorial.weather.services.WeatherService
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.marble.ColdObservable
import io.reactivex.marble.MarbleScheduler
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.reactivestreams.Parser
import java.util.concurrent.TimeUnit

/**
 * Created by Alexandre Delattre on 28/06/2017.
 */
class WeatherViewModelTestWithMarbles {
    lateinit var weatherService: WeatherService
    lateinit var viewModel: WeatherViewModel
    lateinit var scheduler: MarbleScheduler

    val weatherData = WeatherData("toulouse", "", 10f, 20f)
    val bordeauxData = WeatherData("bordeaux", "", 10f, 20f)
    val weatherError = Exception()

    val cityValues = mapOf(
            "t" to "toulouse",
            "b" to "bordeaux"
    )

    val stateValues = mapOf(
            "i" to WeatherViewModel.State.Idle,
            "l" to WeatherViewModel.State.Loading,
            "e" to WeatherViewModel.State.Error(weatherError)
    )

    val searchValues = mapOf("c" to Unit)
    val weatherValues = mapOf(
            "t" to weatherData,
            "b" to bordeauxData
    )

    @Before
    fun setup() {
        weatherService = Mockito.mock(WeatherService::class.java)
        scheduler = MarbleScheduler()
        viewModel = WeatherViewModel(weatherService, scheduler)

    }

    private fun <T> createColdObservable(marbles: String, values: Map<String, T> ? = null, error: Exception? = null): Observable<T> {
        val notifications = Parser.parseMarbles(marbles, values, error, 10)
        return ColdObservable.create<T>(scheduler, notifications)
    }

    @Test
    fun testSuccess() {
        val cityInput =   scheduler.createHotObservable<String>(           "----t-------", cityValues)
        val searchInput = scheduler.createHotObservable<Unit>(             "--------c---", searchValues)
        Mockito.`when`(weatherService.getWeather("toulouse"))
                .thenReturn(createColdObservable<WeatherData>(                     "---(t|)", weatherValues).singleOrError())

        scheduler.expectObservable(viewModel.state).toBe(                  "i-------l--i", stateValues)
        scheduler.expectObservable(viewModel.weather).toBe(                "-----------t", weatherValues)
        cityInput.subscribe(viewModel.city)
        searchInput.subscribe(viewModel.startSearch)
        scheduler.flush()
    }

    @Test
    fun testError() {
        val cityInput =   scheduler.createHotObservable<String>( "----t-------", cityValues)
        val searchInput = scheduler.createHotObservable<Unit>(   "--------c---", searchValues)
        Mockito.`when`(weatherService.getWeather("toulouse"))
                .thenReturn(createColdObservable(                        "---#", weatherValues, weatherError).singleOrError())

        scheduler.expectObservable(viewModel.state).toBe(        "i-------l--e", stateValues)
        scheduler.expectObservable(viewModel.weather).toBe(      "------------", weatherValues)
        cityInput.subscribe(viewModel.city)
        searchInput.subscribe(viewModel.startSearch)
        scheduler.flush()
    }

    @Test
    fun testIgnoreClicks() {
        val cityInput =   scheduler.createHotObservable<String>(           "----t-------", cityValues)
        val searchInput = scheduler.createHotObservable<Unit>(             "c-------cc--", searchValues)
        Mockito.`when`(weatherService.getWeather("toulouse"))
                .thenReturn(createColdObservable<WeatherData>(                     "---(t|)", weatherValues).singleOrError())

        scheduler.expectObservable(viewModel.state).toBe(                  "i-------l--i", stateValues)
        scheduler.expectObservable(viewModel.weather).toBe(                "-----------t", weatherValues)
        cityInput.subscribe(viewModel.city)
        searchInput.subscribe(viewModel.startSearch)
        scheduler.flush()

        Mockito.verify(weatherService, Mockito.times(1)).getWeather("toulouse")
    }

    @Test
    fun testRetry() {
        val cityInput =   scheduler.createHotObservable<String>(           "----t----------", cityValues)
        val searchInput = scheduler.createHotObservable<Unit>(             "--------c-----c", searchValues)
        Mockito.`when`(weatherService.getWeather("toulouse"))
                .thenReturn(createColdObservable(                                   "---#", weatherValues, weatherError).singleOrError())
                .thenReturn(createColdObservable(                                        "---(t|)", weatherValues).singleOrError())

        scheduler.expectObservable(viewModel.state).toBe(                  "i-------l--e--l--i", stateValues)
        scheduler.expectObservable(viewModel.weather).toBe(                "-----------------t", weatherValues)
        cityInput.subscribe(viewModel.city)
        searchInput.subscribe(viewModel.startSearch)
        scheduler.flush()
    }

    @Test
    fun test2Cities() {
        val cityInput =   scheduler.createHotObservable<String>(           "----t--------b-", cityValues)
        val searchInput = scheduler.createHotObservable<Unit>(             "--------c-----c", searchValues)
        Mockito.`when`(weatherService.getWeather("toulouse"))
                .thenReturn(createColdObservable(                                  "--(t|)", weatherValues).singleOrError())
        Mockito.`when`(weatherService.getWeather("bordeaux"))
                .thenReturn(createColdObservable(                                        "--(b|)", weatherValues).singleOrError())

        scheduler.expectObservable(viewModel.state).toBe(                  "i-------l-i---l-i", stateValues)
        scheduler.expectObservable(viewModel.weather).toBe(                "----------t-----b", weatherValues)
        cityInput.subscribe(viewModel.city)
        searchInput.subscribe(viewModel.startSearch)
        scheduler.flush()
    }

}