package com.ibp.mvptutorial.weather.services.weatherapi.openweathermap

import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.github.tomakehurst.wiremock.matching.StringValuePattern
import com.ibp.mvptutorial.weather.buildRetrofit
import com.ibp.mvptutorial.weather.models.WeatherData
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test


/**
 * Created by Alexandre Delattre on 22/06/2017.
 */
class OpenWeatherMapServiceTest {

    @get:Rule
    val wireMockRule = WireMockRule(WireMockConfiguration.options().port(8089))

    lateinit var service: OpenWeatherMapService

    @Before
    fun setUp() {
        stubFor(get(urlPathEqualTo("/weather")).willReturn(aResponse()
                .withStatus(200)
                .withBody(loadResource("weather.json"))
        ))
        val retrofit = buildRetrofit("http://localhost:8089/")
        val api = retrofit.create(OpenWeatherMapApi::class.java)
        service = OpenWeatherMapService(api, "appId")
    }


    @Test
    fun testGetWeather() {
        val data = service.getWeather(1.2, 3.4).blockingGet()
        val expected = WeatherData(
                city="Tawarano",
                pictoUrl="http://openweathermap.org/img/w/01n.png",
                minTemperature=285.514f,
                maxTemperature=285.514f
        )
        assertEquals(expected, data)

        verify(getRequestedFor(urlPathEqualTo("/weather"))
                .withQueryParam("lat", equalTo("1.2"))
                .withQueryParam("lon", equalTo("3.4"))
                .withQueryParam("units", equalTo("metric"))
        )
    }
}


fun Any.loadResource(path: String) =
        javaClass.classLoader.getResourceAsStream(path).bufferedReader().use { it.readText() }
